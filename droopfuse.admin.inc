<?php

/**
 * @file
 * Module admin page callbacks.
 * Robin Bramley, Opsera Ltd.
 */

//////////////////////////////////////////////////////////////////////////////
// droopfuse settings

/**
 * Implements the settings page.
 *
 * @return
 *   The form structure.
 */
function droopfuse_admin_settings() {

  // General settings
  $form['general'] = array('#type' => 'fieldset', '#title' => t('General settings'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  $form['general']['droopfuse_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable LoopFuse lead creation'),
    '#default_value' => DROOPFUSE_ENABLED,
  );
  $form['general']['droopfuse_url'] = array(
    '#type' => 'textfield',
    '#title' => t('LoopFuse URL'),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('LoopFuse endpoint URL e.g. https://webservices.loopfuse.net/webservice/capture'),
    '#default_value' => DROOPFUSE_URL,
  );
  $form['general']['droopfuse_user'] = array(
    '#type' => 'textfield',
    '#title' => t('LoopFuse Username'),
    '#size' => 50,
    '#maxlength' => 255,
    '#default_value' => DROOPFUSE_USER,
  );
  $form['general']['droopfuse_password'] = array(
    '#type' => 'password',
    '#title' => t('LoopFuse password'),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('Password'),
    '#default_value' => DROOPFUSE_PASSWORD,
  );
  $form['general']['droopfuse_formid'] = array(
    '#type' => 'textfield',
    '#title' => t('LoopFuse FormID for the lead capture form'),
    '#size' => 50,
    '#maxlength' => 36,
    '#description' => t('Can obtain this from the LoopFuse lead capture'),
    '#default_value' => DROOPFUSE_FORMID,
  );

  //TODO: custom field config

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['reset'] = array(
    '#type'  => 'submit',
    '#value' => t('Reset to defaults'),
  );
  
  return $form;
}

/**
 * Validate hook for the settings form.
 * /
function droopfuse_admin_settings_validate($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  $values = $form_state['values'];
  switch ($op) {
    case t('Save configuration'):

      $form_state['droopfuse_custom'] = array();
      $droopfuse_custom = TRUE;
      foreach ((trim($values['droopfuse_custom']) ? explode("\n", trim($values['droopfuse_custom'])) : array()) as $line) {
        if (count($data = explode('|', trim($line))) > 1) {
          if ($name = trim(array_shift($data))) {
            $field = array();
            foreach ($data as $pair) {
              list($k, $v) = explode('=>', $pair);
              $field += array('#'. trim($k) => trim($v));
            }
            if (count(array_intersect(array_keys($field), array('#type', '#title'))) == 2)
              $form_state['droopfuse_custom'] += array('custom_'. $name => $field);
            else
              form_set_error('droopfuse_custom', t('type and title are required.'));
          }
          else
            $droopfuse_custom = FALSE;
        }
        else
          $droopfuse_custom = FALSE;
      }
      if (!$droopfuse_custom)
        form_set_error('droopfuse_custom', t('Bad attribute syntax.'));
      break;
  }
}

/**
 * Submit hook for the settings form.
 */
function droopfuse_admin_settings_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  switch ($op) {
    case t('Save configuration'):
      $values = $form_state['values'];

      // General settings.
      variable_set('droopfuse_enabled', $values['droopfuse_enabled']);
      variable_set('droopfuse_user', $values['droopfuse_user']);
      variable_set('droopfuse_password', $values['droopfuse_password']);
      variable_set('droopfuse_url', $values['droopfuse_url']);
      variable_set('droopfuse_formid', $values['droopfuse_formid']);
      
      drupal_set_message(t('The configuration options have been saved.'));
      break;
    case t('Reset to defaults'):
      $values = $form_state['values'];

      // General settings.
      variable_del('droopfuse_enabled');
      variable_del('droopfuse_user');
      variable_del('droopfuse_password');
      variable_del('droopfuse_url');
      variable_del('droopfuse_formid');
      

      drupal_set_message(t('The configuration options have been reset to their default values.'));
      break;
  }

  // Rebuild the menu router.
  menu_rebuild();
}

